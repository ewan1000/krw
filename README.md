# Kent's new website

##### So how does this shit work?

Well, the only part you care about is the content folder. Check in there.

There's folders which contain these .md files -> these files are where your words go.

Note the tag in the first few lines `draft: true` <- this means the page **will not** publish unless you edit that to `draft: false`.

Find the `Edit` button and take a look at the editing toolbar.

If you want to figure our the .md (markdown) syntax, have a look at this site [here](https://www.markdownguide.org).

You can also call me and I'll help you out.

Enjoy.
