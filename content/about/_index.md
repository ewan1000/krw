---
title: "About me"
date: 2021-05-05
draft: false
type: page
#menu: main
featured_image: '/images/kent-candyfloss.jpg'
---

### What can I tell you...

With 10+ years experience in Hospitality from working at the front lines to working from the back office on business strategy...
